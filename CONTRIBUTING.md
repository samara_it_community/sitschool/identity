# Contributing Guide

Solution is build on top of [ory](https://www.ory.sh/) platform and mostly contains configuration
options.

## Commit message

Should match following expression:
```
^(build|chore|ci|docs|feat|fix|perf|refactor|revert|style|test)(\[[A-Za-z0-9\-]+\])?: [\w ]+$
```
to support automatic changes logging in this repository.

Example: `feat[1]: users registration via email and password`.
